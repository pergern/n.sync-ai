import monitor
import socket

monitor = monitor.Monitor()
monitor.start()

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("", 9807))
server_socket.listen(5)

while True:
    client, address = server_socket.accept()
    data = client.recv(1024)
    message = data.decode()

    if message.startswith("0"):
        person_count = monitor.capacity()    
        client.send(str(person_count).encode())
    else:
        timestamp_expiration = int(message)
        monitor.rolling_time_period = timestamp_expiration
