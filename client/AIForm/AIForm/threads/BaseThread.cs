﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AIForm.threads
{
    abstract class BaseThread
    {
        private Thread _thread;

        protected BaseThread()
        {
            _thread = new Thread(new ThreadStart(this.RunThread));
        }

        public void Start() => _thread.Start();
        public void Join()
        {
            try { _thread.Join(); }
            catch (ThreadInterruptedException e)
            {
                _thread.Interrupt();
                _thread.Join();
            }

        }
        public bool IsAlive() => _thread.IsAlive;
        public void Interrupt() => _thread.Interrupt();

        public abstract void RunThread();
    }
}
