﻿using AIForm.data;
using AIForm.network;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AIForm.threads
{
    class QueryCapacityThread : BaseThread
    {
        private ApplicationSettings appSettings = ApplicationSettings.Instance;

        public override void RunThread()
        {
            try
            {
                while (true)
                {
                    if (appSettings.Hostname != null)
                    {
                        TcpClientWrapper tcpClientWrapper = new TcpClientWrapper(appSettings.Hostname, appSettings.Port);
                        bool connected = tcpClientWrapper.Connect();
                        if (connected)
                        {
                            bool success = tcpClientWrapper.Send("0");
                            if (success)
                            {
                                string response = tcpClientWrapper.Recv(128);
                                if (response != null)
                                {
                                    StatusQueue.Enqueue(response);
                                }
                                else
                                {
                                    StatusQueue.Enqueue("Error: Could not recieve message");
                                }
                            }
                            else
                            {
                                StatusQueue.Enqueue("Error: Could not send message");
                            }
                        }
                        else
                        {
                            StatusQueue.Enqueue("Error: Could not connect");
                        }
                    }
                    Thread.Sleep(appSettings.RefreshRate * 1000);
                }
            } catch(Exception)
            {

            }
        }
        public BlockingQueue<string> StatusQueue { get; set; }
    }
}
