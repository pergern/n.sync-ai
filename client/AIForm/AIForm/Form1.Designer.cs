﻿
namespace AIForm
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.statusPage = new System.Windows.Forms.TabPage();
            this.capcityLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.settingsPage = new System.Windows.Forms.TabPage();
            this.refreshRateText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.applySettingsButton = new System.Windows.Forms.Button();
            this.timestampExpirationText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.maxCapacityText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.serverAddressText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.warningLabel = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.statusPage.SuspendLayout();
            this.settingsPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.statusPage);
            this.tabControl.Controls.Add(this.settingsPage);
            this.tabControl.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(513, 417);
            this.tabControl.TabIndex = 0;
            // 
            // statusPage
            // 
            this.statusPage.Controls.Add(this.warningLabel);
            this.statusPage.Controls.Add(this.capcityLabel);
            this.statusPage.Controls.Add(this.label5);
            this.statusPage.Controls.Add(this.statusLabel);
            this.statusPage.Location = new System.Drawing.Point(4, 25);
            this.statusPage.Name = "statusPage";
            this.statusPage.Padding = new System.Windows.Forms.Padding(3);
            this.statusPage.Size = new System.Drawing.Size(505, 388);
            this.statusPage.TabIndex = 0;
            this.statusPage.Text = "Status";
            this.statusPage.UseVisualStyleBackColor = true;
            // 
            // capcityLabel
            // 
            this.capcityLabel.AutoSize = true;
            this.capcityLabel.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.capcityLabel.Location = new System.Drawing.Point(244, 48);
            this.capcityLabel.Name = "capcityLabel";
            this.capcityLabel.Size = new System.Drawing.Size(130, 28);
            this.capcityLabel.TabIndex = 2;
            this.capcityLabel.Text = "CAPACITY";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(12, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(216, 28);
            this.label5.TabIndex = 1;
            this.label5.Text = "Current Capacity:";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Lucida Sans Unicode", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.statusLabel.Location = new System.Drawing.Point(12, 9);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(43, 16);
            this.statusLabel.TabIndex = 0;
            this.statusLabel.Text = "label4";
            // 
            // settingsPage
            // 
            this.settingsPage.Controls.Add(this.refreshRateText);
            this.settingsPage.Controls.Add(this.label7);
            this.settingsPage.Controls.Add(this.applySettingsButton);
            this.settingsPage.Controls.Add(this.timestampExpirationText);
            this.settingsPage.Controls.Add(this.label3);
            this.settingsPage.Controls.Add(this.maxCapacityText);
            this.settingsPage.Controls.Add(this.label2);
            this.settingsPage.Controls.Add(this.serverAddressText);
            this.settingsPage.Controls.Add(this.label1);
            this.settingsPage.Location = new System.Drawing.Point(4, 25);
            this.settingsPage.Name = "settingsPage";
            this.settingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.settingsPage.Size = new System.Drawing.Size(505, 388);
            this.settingsPage.TabIndex = 1;
            this.settingsPage.Text = "Settings";
            this.settingsPage.UseVisualStyleBackColor = true;
            // 
            // refreshRateText
            // 
            this.refreshRateText.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.refreshRateText.Location = new System.Drawing.Point(271, 152);
            this.refreshRateText.Name = "refreshRateText";
            this.refreshRateText.Size = new System.Drawing.Size(185, 44);
            this.refreshRateText.TabIndex = 8;
            this.refreshRateText.Text = "20";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(271, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 28);
            this.label7.TabIndex = 7;
            this.label7.Text = "Refresh Rate";
            // 
            // applySettingsButton
            // 
            this.applySettingsButton.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.applySettingsButton.Location = new System.Drawing.Point(160, 330);
            this.applySettingsButton.Name = "applySettingsButton";
            this.applySettingsButton.Size = new System.Drawing.Size(142, 40);
            this.applySettingsButton.TabIndex = 6;
            this.applySettingsButton.Text = "Apply";
            this.applySettingsButton.UseVisualStyleBackColor = true;
            this.applySettingsButton.Click += new System.EventHandler(this.applySettingsButton_Click);
            // 
            // timestampExpirationText
            // 
            this.timestampExpirationText.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.timestampExpirationText.Location = new System.Drawing.Point(3, 255);
            this.timestampExpirationText.Name = "timestampExpirationText";
            this.timestampExpirationText.Size = new System.Drawing.Size(185, 44);
            this.timestampExpirationText.TabIndex = 5;
            this.timestampExpirationText.Text = "60";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(3, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(267, 28);
            this.label3.TabIndex = 4;
            this.label3.Text = "Timestamp Expiration";
            // 
            // maxCapacityText
            // 
            this.maxCapacityText.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.maxCapacityText.Location = new System.Drawing.Point(3, 152);
            this.maxCapacityText.Name = "maxCapacityText";
            this.maxCapacityText.Size = new System.Drawing.Size(185, 44);
            this.maxCapacityText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(3, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 28);
            this.label2.TabIndex = 2;
            this.label2.Text = "Max Capacity";
            // 
            // serverAddressText
            // 
            this.serverAddressText.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.serverAddressText.Location = new System.Drawing.Point(3, 45);
            this.serverAddressText.Name = "serverAddressText";
            this.serverAddressText.Size = new System.Drawing.Size(499, 44);
            this.serverAddressText.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Sans Unicode", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server Address";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Font = new System.Drawing.Font("Lucida Sans Unicode", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.warningLabel.ForeColor = System.Drawing.Color.Red;
            this.warningLabel.Location = new System.Drawing.Point(134, 139);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(240, 35);
            this.warningLabel.TabIndex = 3;
            this.warningLabel.Text = "OVER CAPACITY";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 437);
            this.Controls.Add(this.tabControl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.statusPage.ResumeLayout(false);
            this.statusPage.PerformLayout();
            this.settingsPage.ResumeLayout(false);
            this.settingsPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage statusPage;
        private System.Windows.Forms.Label capcityLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.TabPage settingsPage;
        private System.Windows.Forms.TextBox refreshRateText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button applySettingsButton;
        private System.Windows.Forms.TextBox timestampExpirationText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox maxCapacityText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serverAddressText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label warningLabel;
    }
}

