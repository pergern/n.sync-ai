﻿using AIForm.data;
using AIForm.network;
using AIForm.threads;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace AIForm
{
    public partial class Form1 : Form
    {
        private ApplicationSettings appSettings = ApplicationSettings.Instance;
        private QueryCapacityThread queryCapacityThread = new QueryCapacityThread();
        private BlockingQueue<string> statusQueue = new BlockingQueue<string>(30);


        public Form1()
        {
            InitializeComponent();
            warningLabel.Visible = false;

            appSettings.Port = 9807;
            appSettings.RefreshRate = 1;
            appSettings.TimestampExpiration = 60;

            queryCapacityThread.StatusQueue = statusQueue;
            queryCapacityThread.Start();

            statusLabel.Text = "";
            
            
        }

        private void applySettingsButton_Click(object sender, EventArgs e)
        {
            string ipAddress = serverAddressText.Text;
            string capacity = maxCapacityText.Text;
            string refreshRate = refreshRateText.Text;
            string timestampExpiration = timestampExpirationText.Text;

            appSettings.Hostname = ipAddress;
            appSettings.MaxCapacity = int.Parse(capacity);
            appSettings.RefreshRate = int.Parse(refreshRate);

            if(int.Parse(timestampExpiration) != appSettings.TimestampExpiration)
            {
                UpdateExpirationThread updateExpirationThread = new UpdateExpirationThread();
                updateExpirationThread.TimestampExpiration = timestampExpiration;
                updateExpirationThread.Start();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // this.statusLabel.Text = "Update: " + updateCount++;
            if (!statusQueue.IsEmpty())
            {
                string[] statusMessages = statusQueue.ToArrayAndEmpty();
                string newestStatusMessage = null, newestCapacity = null;
                foreach (string status in statusMessages)
                {
                    if (!status.Contains("Error"))
                    {
                        newestCapacity = status;
                    }
                    else
                    {
                        newestStatusMessage = status;
                    }
                }

                if (newestCapacity != null)
                {
                    capcityLabel.Text = newestCapacity;
                    int capcity = int.Parse(newestCapacity);
                    if(capcity >= appSettings.MaxCapacity)
                    {
                        warningLabel.Visible = true;
                    }
                    else
                    {
                        warningLabel.Visible = false;
                    }
                }
                if (newestStatusMessage != null) statusLabel.Text = newestStatusMessage;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.queryCapacityThread.Interrupt();
            this.queryCapacityThread.Join();
        }
    }
}
