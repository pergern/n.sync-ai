﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIForm.data
{
    sealed class ApplicationSettings
    {
        private static ApplicationSettings instance = null;
        private static object padlock = new object();
        ApplicationSettings() {}

        public static ApplicationSettings Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null) instance = new ApplicationSettings();
                    return instance;
                }
            }
        }

        public string Hostname { get; set; }
        public int MaxCapacity { get; set; }
        public int Port { get; set; }
        public int RefreshRate { get; set; }
        public int TimestampExpiration { get; set; }

    }
}
