﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace AIForm.data
{
    class BlockingQueue<T>
    {
        private readonly Queue<T> queue = new Queue<T>();
        private readonly int maxSize;
        private bool closing, voidQueue = false, limitedQueue = false;
        public BlockingQueue(int maxSize)
        {
            if (maxSize == 0) maxSize = Int32.MaxValue;
            else if (maxSize == -1) voidQueue = true;
            else if (maxSize < 0) { limitedQueue = true; maxSize = maxSize * -1; }
            this.maxSize = maxSize;
            this.closing = false;
        }

        public void Clear()
        {
            lock (queue)
            {
                queue.Clear();
                Monitor.PulseAll(queue);
            }

        }

        public void Close()
        {
            lock (queue)
            {
                closing = true;
                Monitor.PulseAll(queue);
            }
        }
        public bool Enqueue(T item)
        {
            if (this.voidQueue) return false;
            lock (queue)
            {
                if (this.queue.Count >= maxSize && this.limitedQueue)
                {
                    this.queue.Dequeue();
                    this.queue.Enqueue(item);
                    return true;
                }
                while (queue.Count >= maxSize && !this.closing)
                {
                    Monitor.Wait(queue);
                }
                if (this.closing) return false;
                queue.Enqueue(item);
                if (queue.Count == 1)
                {
                    Monitor.PulseAll(queue);
                }
                return true;
            }
        }

        public T[] ToArray()
        {
            return this.queue.ToArray();
        }

        public T[] ToArrayAndEmpty()
        {
            lock (queue)
            {
                T[] array = this.queue.ToArray();
                this.queue.Clear();
                return array;
            }
        }

        public int Count()
        {
            return this.queue.Count;
        }

        public int Size
        {
            get { return this.maxSize; }
        }

        public bool TryDequeue(out T value)
        {
            lock (queue)
            {
                while (queue.Count == 0)
                {
                    if (this.closing)
                    {
                        value = default(T);
                        return false;
                    }
                    Monitor.Wait(queue);
                }
                value = queue.Dequeue();
                if (queue.Count == maxSize - 1)
                {
                    Monitor.PulseAll(queue);
                }
                return true;
            }
        }

        public bool IsEmpty()
        {
            return queue.Count == 0;
        }
    }
}
